What is react and what problems does it try to solve?

React is a javascript library that simplifies and abstracts away some of the syntax of vanilla javascript. It uses a virtual DOM to help break projects down into bite-sized components. Using this virtual DOM, the developer can easily create UI elements.